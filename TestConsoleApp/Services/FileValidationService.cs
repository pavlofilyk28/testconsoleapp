﻿using System.Collections.Concurrent;
using System.IO;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using TestConsoleApp.DTOs;

namespace TestConsoleApp.Services
{
    public static class FileValidationService
    {
        private readonly static string _validItemRegex = @"^-?\d+((\.+\d+)|(\d?))$";
        public static ConcurrentBag<RowConditionDTO> ValidateFile(string path)
        {
            string[] lines = File.ReadAllLines(path);
            var rowConditionModels = new ConcurrentBag<RowConditionDTO>();
            Parallel.ForEach(lines, (line, state, index) =>
            {
                ParseLine(line, index, rowConditionModels);
            });

            return rowConditionModels;
        }

        private static void ParseLine(string line, long lineIndex, ConcurrentBag<RowConditionDTO> rowConditionModels)
        {
            var items = line.Split(',');
            float sum = 0;
            bool hasInvalidItems = false;
            bool atLeastOneValidItem = false;
            foreach (var item in items)
            {
                if (!Regex.IsMatch(item.Trim(' '), _validItemRegex))
                {
                    hasInvalidItems = true;
                }
                else
                {
                    atLeastOneValidItem = true;
                    sum += float.Parse(item);
                }
            }
            rowConditionModels.Add(new RowConditionDTO()
            {
                Sum = atLeastOneValidItem ? sum : (float?)null,
                RowNumber = lineIndex + 1,
                HasInvalidItems = hasInvalidItems
            });
        }
    }
}
