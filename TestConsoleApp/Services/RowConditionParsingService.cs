﻿using System.Collections.Generic;
using System.Linq;
using TestConsoleApp.DTOs;

namespace TestConsoleApp.Services
{
    public static class RowConditionParsingService
    {
        public static RowConditionDTO GetRowWithMaxSum(IEnumerable<RowConditionDTO> rowConditionModels)
        {
            var maxSum = rowConditionModels.Where(x => x.Sum.HasValue)
                    .OrderByDescending(x => x.Sum)
                    .ThenBy(x => x.RowNumber).FirstOrDefault();

            return maxSum;
        }

        public static IEnumerable<long> GetInvalidRowsIds(IEnumerable<RowConditionDTO> rowConditionModels)
        {
            var invalidRows = rowConditionModels.Where(x => x.HasInvalidItems)
                    .Select(x => x.RowNumber);

            return invalidRows;
        }
    }
}
