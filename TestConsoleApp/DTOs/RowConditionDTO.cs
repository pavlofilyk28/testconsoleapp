﻿namespace TestConsoleApp.DTOs
{
    public class RowConditionDTO
    {
        public float? Sum { get; set; }
        public long RowNumber { get; set; }
        public bool HasInvalidItems { get; set; }
    }
}
