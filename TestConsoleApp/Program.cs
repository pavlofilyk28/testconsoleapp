﻿using System;
using System.Linq;
using TestConsoleApp.Services;

namespace TestConsoleApp
{
    public class Program
    {
        public static void Main(string[] args)
        {
            try
            {
                string path = args.Length > 0 ? args[0] : "";
                if (string.IsNullOrEmpty(path))
                {
                    Console.WriteLine("Enter file path");
                    path = Console.ReadLine();
                }

                var rowConditionModels = FileValidationService.ValidateFile(path);

                var maxSum = RowConditionParsingService.GetRowWithMaxSum(rowConditionModels);
                var invalidRows = RowConditionParsingService.GetInvalidRowsIds(rowConditionModels);                

                string maxSumMessage = maxSum != null
                    ? $"Max sum = {maxSum.Sum}; Row number = {maxSum.RowNumber}"
                    : "Can't calculate max sum";

                var invalidRowsMessage = invalidRows.Count() > 0
                    ? $"Invalid rows: {string.Join(",", invalidRows)}"
                    : "All file is invalid";

                Console.WriteLine(maxSumMessage);
                Console.WriteLine(invalidRowsMessage);
                Console.ReadKey();
            }
            catch (Exception e)
            {
                Console.WriteLine($"ERROR: {e.Message}; Inner error: {e.InnerException.Message}");
            }            
        }
    }

    
}
