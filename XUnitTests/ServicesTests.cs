﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using TestConsoleApp.DTOs;
using TestConsoleApp.Services;
using Xunit;

namespace XUnitTests
{
    public class ServicesTests
    {
        #region FileValidationService
        [Fact]
        public void ValidateFile_FileNotExists()
        {
            var path = $"{Environment.CurrentDirectory}\\Resources\\test_notexists.txt";

            Assert.Throws<FileNotFoundException>(() => { FileValidationService.ValidateFile(path); });
        }

        [Fact]
        public void ValidateFile_DirectoryNotExists()
        {
            var path = $"{Environment.CurrentDirectory}\\Resources\\TEST folder\\test_notexists.txt";

            Assert.Throws<DirectoryNotFoundException>(() => { FileValidationService.ValidateFile(path); });
        }

        [Fact]
        public void ValidateFile_EmptyFileParsing()
        {
            var path = $"{Environment.CurrentDirectory}\\Resources\\Empty.txt";

            var result = FileValidationService.ValidateFile(path);

            Assert.Empty(result);
        }

        [Fact]
        public void ValidateFile_NonEmptyFileParsing()
        {
            var path = $"{Environment.CurrentDirectory}\\Resources\\NotEmpty.txt";

            var result = FileValidationService.ValidateFile(path);

            Assert.NotEmpty(result);
        }

        [Fact]
        public void ValidateFile_InvalidRowsParsing()
        {
            var path = $"{Environment.CurrentDirectory}\\Resources\\NotEmpty.txt";

            var result = FileValidationService.ValidateFile(path);

            Assert.True(result.Where(x => x.RowNumber == 1).First().HasInvalidItems); Assert.True(result.Where(x => x.RowNumber == 1).First().HasInvalidItems);
            Assert.False(result.Where(x => x.RowNumber == 2).First().HasInvalidItems);
            Assert.False(result.Where(x => x.RowNumber == 3).First().HasInvalidItems);
            Assert.True(result.Where(x => x.RowNumber == 4).First().HasInvalidItems); Assert.True(result.Where(x => x.RowNumber == 1).First().HasInvalidItems);
            Assert.True(result.Where(x => x.RowNumber == 5).First().HasInvalidItems); Assert.True(result.Where(x => x.RowNumber == 1).First().HasInvalidItems);
        }

        [Fact]
        public void ValidateFile_SumParsing()
        {
            var path = $"{Environment.CurrentDirectory}\\Resources\\NotEmpty.txt";

            var result = FileValidationService.ValidateFile(path);

            Assert.Equal(39.9231f, result.Where(x => x.RowNumber == 1).First().Sum);
            Assert.Equal(601f, result.Where(x => x.RowNumber == 2).First().Sum);
            Assert.Equal(936.22f, result.Where(x => x.RowNumber == 3).First().Sum);
            Assert.Equal(159.66f, result.Where(x => x.RowNumber == 4).First().Sum);
            Assert.Equal(891.22f, result.Where(x => x.RowNumber == 5).First().Sum);
        }

        [Fact]
        public void ValidateFile_InvalidRowsSumParsing()
        {
            var path = $"{Environment.CurrentDirectory}\\Resources\\InvalidRows.txt";

            var result = FileValidationService.ValidateFile(path);

            Assert.True(result.Where(x => x.RowNumber == 1).First().HasInvalidItems);
            Assert.Null(result.Where(x => x.RowNumber == 1).First().Sum);
            Assert.True(result.Where(x => x.RowNumber == 2).First().HasInvalidItems);
            Assert.Null(result.Where(x => x.RowNumber == 2).First().Sum);
            Assert.True(result.Where(x => x.RowNumber == 3).First().HasInvalidItems);
            Assert.Null(result.Where(x => x.RowNumber == 3).First().Sum);
        }

        [Fact]
        public void ValidateFile_NegativeItemsSumParsing()
        {
            var path = $"{Environment.CurrentDirectory}\\Resources\\NegativeItems.txt";

            var result = FileValidationService.ValidateFile(path);

            Assert.Equal(8.9f, result.Where(x => x.RowNumber == 1).First().Sum);
            Assert.Equal(-7f, result.Where(x => x.RowNumber == 2).First().Sum);
            Assert.Equal(462f, result.Where(x => x.RowNumber == 3).First().Sum);
            Assert.Equal(-5f, result.Where(x => x.RowNumber == 4).First().Sum);
        }
        #endregion

        #region RowConditionParsingService
        [Fact]
        public void GetRowWithMaxSum_EmptyRowDTOList()
        {
            var rowConditionModels = new List<RowConditionDTO>();

            var result = RowConditionParsingService.GetRowWithMaxSum(rowConditionModels);

            Assert.Null(result);
        }

        [Fact]
        public void GetRowWithMaxSum_RowDTOListWithoutSumValues()
        {
            var rowConditionModels = new List<RowConditionDTO>()
            {
                new RowConditionDTO { RowNumber = 1, Sum = null },
                new RowConditionDTO { RowNumber = 2, Sum = null }
            };

            var result = RowConditionParsingService.GetRowWithMaxSum(rowConditionModels);

            Assert.Null(result);
        }

        [Fact]
        public void GetRowWithMaxSum_MaxSumCalculation()
        {
            var rowConditionModels = new List<RowConditionDTO>()
            {
                new RowConditionDTO { RowNumber = 1, Sum = -5.7f },
                new RowConditionDTO { RowNumber = 2, Sum = null },
                new RowConditionDTO { RowNumber = 3, Sum = 3.8f },
                new RowConditionDTO { RowNumber = 4, Sum = 3.8f },
                new RowConditionDTO { RowNumber = 5, Sum = 0f },
            };

            var result = RowConditionParsingService.GetRowWithMaxSum(rowConditionModels);

            Assert.Equal(3.8f, result.Sum);
            Assert.Equal(3, result.RowNumber);
        }

        [Fact]
        public void GetInvalidRowsIds_EmptyRowDTOList()
        {
            var rowConditionModels = new List<RowConditionDTO>();

            var result = RowConditionParsingService.GetInvalidRowsIds(rowConditionModels);

            Assert.Empty(result);
        }

        [Fact]
        public void GetInvalidRowsIds_RowDTOListWithoutInvalid()
        {
            var rowConditionModels = new List<RowConditionDTO>()
            {
                new RowConditionDTO { RowNumber = 1, HasInvalidItems = false },
                new RowConditionDTO { RowNumber = 2, HasInvalidItems = false },
                new RowConditionDTO { RowNumber = 3, HasInvalidItems = false }
            };

            var result = RowConditionParsingService.GetInvalidRowsIds(rowConditionModels);

            Assert.Empty(result);
        }

        [Fact]
        public void GetInvalidRowsIds_InvalidRowIdsCalculation()
        {
            var rowConditionModels = new List<RowConditionDTO>()
            {
                new RowConditionDTO { RowNumber = 1, HasInvalidItems = true },
                new RowConditionDTO { RowNumber = 2, HasInvalidItems = false },
                new RowConditionDTO { RowNumber = 3, HasInvalidItems = true },
                new RowConditionDTO { RowNumber = 4, HasInvalidItems = false },
                new RowConditionDTO { RowNumber = 5, HasInvalidItems = false }
            };

            var result = RowConditionParsingService.GetInvalidRowsIds(rowConditionModels);

            Assert.Equal(1, result.ElementAt(0));
            Assert.Equal(3, result.ElementAt(1));
            Assert.Equal(2, result.Count());
        }
        #endregion
    }
}
